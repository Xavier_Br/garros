﻿angular.module('myApp.match.controller', ['ngRoute', 'myApp.match.service'])

.controller('MatchCtrl', ['$scope', 'terrainService', 'joueurService', 'arbitreService', function ($scope, terrainService, joueurService, arbitreService) {
    $scope.match = {};
    $scope.listeJoueurs = joueurService.lister();
    $scope.listeArbitres = arbitreService.lister();
    $scope.listeTerrains = terrainService.lister();
}]);