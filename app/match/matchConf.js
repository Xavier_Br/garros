'use strict';

angular.module('myApp.match', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/match', {
    templateUrl: 'match/match.html',
    controller: 'MatchCtrl'
  });
}])

.controller('MatchCtrl', ['$scope', function($scope) {
    $scope.match = {};
    $scope.listeJoueurs = $T.getService('joueurService').lister();
    $scope.listeArbitres = $T.getService('arbitreService').lister();
    $scope.listeTerrains = $T.getService('terrainService').lister();
}]);