'use strict';

angular.module('myApp.match', ['ngRoute', 'myApp.match.controller'])

.config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/match', {
		templateUrl: 'match/match.html',
		controller: 'MatchCtrl'
	});
}])



