'use strict';

angular.module('myApp.joueur', ['ngRoute', 'myApp.joueur.controller'])

.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/joueur/:id', {
        templateUrl: 'joueur/joueur.html',
        controller: 'JoueurCtrl'
    })
    .when('/joueur', {
        templateUrl: 'joueur/joueur.html',
        controller: 'JoueurCtrl'
    });
}])



