﻿angular.module('myApp.joueur.controller', ['ngRoute', 'myApp.joueur.service'])

.controller('JoueurCtrl', ['$scope','$routeParams','joueurService', function ($scope, $routeParams,joueurService) {
    $scope.joueur = joueurService.chercher($routeParams.id) || {};
    //$scope.joueur = {};

    $scope.inscrire = function () {
        joueurService.ajouter($scope.joueur);
    }
}]);