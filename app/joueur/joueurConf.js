'use strict';

angular.module('myApp.joueur', ['ngRoute'])

.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/joueur/:id', {
        templateUrl: 'joueur/joueur.html',
        controller: 'JoueurCtrl'
    })
    .when('/joueur', {
        templateUrl: 'joueur/joueur.html',
        controller: 'JoueurCtrl'
    });
}])

.controller('JoueurCtrl', ['$scope','$routeParams', function ($scope, $routeParams) {
    $scope.joueur = $T.getService('joueurService').chercher($routeParams.id) || {};
    //$scope.joueur = {};

    $scope.inscrire = function () {
        $T.getService("joueurService").ajouter($scope.joueur);
    }
}]);