﻿//Event

var joueurController = {
  confirmer : function () {
    joueur = new Joueur(
     $T.query("#id_joueur").value,
     $T.query("#nom_joueur").value,
     $T.query("#prenom_joueur").value,
     $T.query("#sexe_joueur").value,
     $T.query("#classementatp").value,
     $T.query("#nationalite_joueur").value,
     $T.query("#age_joueur").value,
     $T.query("#nb_match_gagne").value,
     $T.query("#nb_ace").value,
     $T.query("#vitesse_moy").value);
    $T.getService("joueurService").ajouter(joueur);
    $T.query("#joueurMatch").innerHTML += "<option value=\"" + joueur.id + "\">" + joueur.nom + " " + joueur.prenom + "</option>";
  },
  init: function () {

    var toto = function(text){
                  $T.query("#joueur").innerHTML=text;
                  $T.bind('#confirmerjoueur',['click'], this.confirmer);
                };
    $T.route("formjoueur.js",toto);
  }
}

var arbitreController = {   
  confirmer : function () {
    arbitre = new Arbitre(
     $T.query("#id_arbitre").value,
     $T.query("#nom_arbitre").value,
     $T.query("#prenom_arbitre").value,
     $T.query("#sexe_arbitre").value,
     $T.query("#age_arbitre").value,
     $T.query("#nationalite_arbitre").value,
     $T.query("#nb_match_arbitre").value,
     $T.query("#indice_exp").value);
    $T.getService("arbitreService").ajouter(arbitre);
    $T.query("#arbitreMatch").innerHTML += "<option value=\"" + arbitre.id + "\">" + arbitre.nom + " " + arbitre.prenom + "</option>";
  },
  init: function () {
    optionsArbitre = [];
    arbitre = $T.persistences("storage").loadDate("arbitres");
    optionElementGenerique(arbitres, optionsArbitre, "#arbitreMatch", ["id", "nom", "prenom"]);
       $T.bind('#confirmerarbitre', ['click'], this.confirmer);  // click pour déclencher la fonction
     }
   }


   var terrainController = {
    confirmer : function () {
      terrain = new Terrain(
       $T.query("#id_terrain").value,
       $T.query("#lieu").value,
       $T.query("#nom_terrain").value,
       $T.query("#type_surface").value,
       $T.query("#nb_spectateur").value);
      $T.getService("terrainService").ajouter(terrain);
      $T.query("#terrainMatch").innerHTML += "<option value=\"" + terrain.id + "\">" + terrain.lieu + " " + terrain.nom + "</option>";
    },
    init: function () {
       $T.bind('#confirmerterrain', ['click'], this.confirmer);  // click pour déclencher la fonction
     }
   }

/* var matchController = {
    confirmer: function () {
        match = new Match(
           $T.query("#id_match").value,
             $T.query("#type_match").value,
             $T.query("#joueur1").value,
             $T.query("#joueur2").value,
             $T.query("#joueur3").value,
             $T.query("#joueur4").value,
             $T.query("#terrain").value);
        $T.getServices("matchService").ajouter(match);
        $T.query("#matchMatch").innerHTML += "<option value=\"" + match.id + "\">" + match.type_match + " " + match.joueur1 + "</option>";
    },
    init : function(){
                optionsJoueur = [];
          joueur = $T.persistences("storage").loadDate("joueurs");
          optionElementGenerique(joueurs, optionsJoueur, "#joueurMatch", ["id", "nom", "prenom"]);

    tennisFW.core.bind('#confirmermatch',['click'], this.confirmer);  // click pour déclencher la fonction
}
} */

$T.controllers["joueurController"] = joueurController;
$T.controllers["arbitreController"] = arbitreController;
$T.controllers["terrainController"] = terrainController;
//tennisFW.controllers["matchController"] = matchController;