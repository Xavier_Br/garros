﻿// les constructeurs

function Joueur(Pid, Pnom, Pprenom, Psexe, Pclassementatp, Pnationalite, Page, Pnb_match_gagne, Pnb_ace, Pvitesse_moy) {
    this.id = Pid;
    this.nom = Pnom;
    this.prenom = Pprenom;
    this.sexe = Psexe;
    this.classementatp = Pclassementatp;
    this.nationalite = Pnationalite;
    this.age = Page;
    this.nb_match_gagne = Pnb_match_gagne;
    this.nb_ace = Pnb_ace;
    this.vitesse_moy = Pvitesse_moy;
}

function Arbitre(Pid, Pnom, Pprenom, Psexe, Page, Pnationalite, Pnb_match_arbitre, Pindice_experience) {
    this.id = Pid;
    this.nom = Pnom;
    this.prenom = Pprenom;
    this.sexe = Psexe;
    this.age = Page;
    this.nationalite = Pnationalite;
    this.nb_match_arbitre = Pnb_match_arbitre;
    this.Pindice_experience;
}


function Terrain(Pid, Plieu, Pnom, Ptype_surface, Pnb_spectateur) {
    this.id = Pid;
    this.lieu = Plieu;
    this.nom = Pnom;
    this.type_surface = Ptype_surface;
    this.nb_spectateur = Pnb_spectateur;
}


/*function Match(Pid, Ptype_match, Pjoueur1, Pjoueur2, Pjoueur3, Pjoueur4, Pterrain, Pscore) {
    this.id = Pid;
    this.type_match = Ptype_match;
    this.joueur1 = Pjoueur1;
    this.joueur2 = Pjoueur2;
    this.joueur3 = Pjoueur3;
    this.joueur4 = Pjoueur4;
    this.terrain = Pterrain;
} */