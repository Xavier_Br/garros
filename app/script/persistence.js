﻿var storage = {

    saveOnLStorage: function (key, value) {
        var listeElement = this.loadData(key);
        listeElement[value.id] = value;
        localStorage.setItem(key, JSON.stringify(listeElement));
        return listeElement;
    },

    deleteOnLStorage: function (key, value) {
        var listeElement = this.loadData(key);
        delete listeElement[value.id];
        localStorage.setItem(key, JSON.stringify(listeElement));
        return listeElement;
    },

    deleteAll: function (key) {
        localStorage.removeItem(key);
        return {};
    },

    loadData: function (key) {
        var data = JSON.parse(localStorage.getItem(key)) || {};
        return data;
    }
}

$T.persistences["storage"] = storage;