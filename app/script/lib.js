﻿var joueurs;
var arbitres;
var terrains;
var matchs;
var optionsArbitre;
var optionsJoueur;
var optionsTerrain;
var optionsMatch;

var optionElementGenerique = function (listeElements, optionsElement, selector, params) {
    for (var k in listeElements) {
        var element = listeElements[k];
        optionsElement.push("<option value=\"" + element[params[0]] + "\">" + element[params[1]] + " " + element[params[2]] + "</option>");
    };
    tennisFW.core.query(selector).innerHTML += optionsElement.join("");
}

function init() {
   joueurs = tennisFW.persistences["storage"].loadData('joueurs');
   arbitres = tennisFW.persistences["storage"].loadData('arbitres');
   terrains = tennisFW.persistences["storage"].loadData('terrains');
   //matchs = tennisFW.persistences["storage"].loadData('matchs') || {};
   
   optionsJoueur = [];
   optionsArbitre = [];
   optionTerrain = [];
   //optionMatch = [];
   

   optionElementGenerique(arbitres, optionsArbitre, "#arbitreMatch", ["id","nom","prenom"]);
   optionElementGenerique(joueurs, optionsJoueur, "#joueurMatch", ["id", "nom", "prenom"]);
   optionElementGenerique(terrains, optionTerrain, "#terrainMatch", ["id", "lieu", "nom"]);
   //optionElementGenerique(match, optionMatch, "#match", ["id", "type_match", "joueur1"]);
}