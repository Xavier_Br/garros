﻿var tennisFW = {

	query: function (selector) {
		return document.querySelector(selector);
	},
	bind: function (selector, evenements, comportement) {
		var binding = this.query(selector);
		for (i = 0; i < evenements.length; i++); {
			binding.addEventListener(evenements[i], comportement);
		}
	},
	route: function(template,fonction){
		var rawFile = new XMLHttpRequest();
		rawFile.open("GET", template, false);
		rawFile.onreadystatechange = function ()
		{
			if(rawFile.readyState === 4)
			{
				if(rawFile.status === 200 || rawFile.status == 0)
				{
					var allText = rawFile.responseText;
					fonction(allText);
				}
			}
		}
		rawFile.send(null);
	},

	getController: function (unController) {
		return this.controllers[unController];
	},
	getService: function (unService) {
		return this.services[unService];
	},
	getPersistence: function (unePersistence) {
		return this.persistences[unePersistence];
	},


	persistences: {},
	services: {},
	controllers: {},
}


var $T = tennisFW;