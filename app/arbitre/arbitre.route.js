'use strict';

angular.module('myApp.arbitre', ['ngRoute', 'myApp.arbitre.controller'])

.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/arbitre', {
        templateUrl: 'arbitre/arbitre.html',
        controller: 'ArbitreCtrl'
    });
}])

