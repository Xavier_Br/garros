'use strict';

angular.module('myApp.arbitre', ['ngRoute'])

.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/arbitre', {
        templateUrl: 'arbitre/arbitre.html',
        controller: 'ArbitreCtrl'
    });
}])

.controller('ArbitreCtrl', ['$scope', function ($scope) {
    $scope.inscrire = function () {
        $T.getService("arbitreService").ajouter($scope.arbitre);
    }

}]);