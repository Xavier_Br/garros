﻿'use strict';

angular.module('myApp.arbitre.controller', ['ngRoute', 'myApp.arbitre.service'])

.controller('ArbitreCtrl', ['$scope', 'arbitreService', function ($scope, arbitreService) {
    $scope.inscrire = function () {
        arbitreService.ajouter($scope.arbitre);
    }

}]);