﻿angular.module('myApp.terrain.controller', ['ngRoute', 'myApp.terrain.service'])

.controller('TerrainCtrl', ['$scope', 'terrainService', function ($scope, terrainService) {
    $scope.inscrire = function () {
        terrainService.ajouter($scope.terrain);
    }
}]);