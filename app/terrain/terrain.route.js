'use strict';

angular.module('myApp.terrain', ['ngRoute', 'myApp.terrain.controller'])

.config(['$routeProvider', function ($routeProvider) {
	$routeProvider.when('/terrain', {
		templateUrl: 'terrain/terrain.html',
		controller: 'TerrainCtrl'
	});
}])


