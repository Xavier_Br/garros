'use strict';

angular.module('myApp.terrain', ['ngRoute'])

.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/terrain', {
        templateUrl: 'terrain/terrain.html',
        controller: 'TerrainCtrl'
    });
}])

.controller('TerrainCtrl', ['$scope', function ($scope) {
    $scope.inscrire = function () {
        $T.getService("terrainService").ajouter($scope.terrain);
    }
}]);