﻿'use strict';

angular.module('myApp.match.service', [])

.factory('arbitreService', function () {
    var pers = $T.getPersistence("storage");
    var arbitreService = {

        ajouter: function (arbitreosef) {
            pers.saveOnLStorage('arbitres', arbitreosef);
        },

        retirer: function (arbitreosef) {
            pers.deleteOnLStorage('arbitres', arbitreosef);
        },

        retirerTout: function () {
            pers.deleteAll('arbitres');
        },
        lister: function () {
            return pers.loadData('arbitres');
        },
    };
    return arbitreService;
})

.factory('joueurService', function () {
    var pers = $T.getPersistence("storage");
    var joueurService = {

        ajouter: function (joueurosef) {
            pers.saveOnLStorage('joueurs', joueurosef);
        },

        retirer: function (joueurosef) {
            return pers.deleteOnLStorage('joueurs', joueurosef);
        },
        retirerTout: function () {
            pers.deleteAll('joueurs');
        },
        lister: function () {
            return pers.loadData('joueurs');
        },
        chercher: function (joueurid) {
            return this.lister()[joueurid];
        },
        compter: function (list) {
            var data = [0, 0, 0, 0];
            var age = 0;
            for (var id in list) {
                age = list[id].age;
                if (age > 10 && age <= 20) { data[0]++; }
                else if (age > 20 && age <= 30) { data[1]++; }
                else if (age > 30 && age <= 40) { data[2]++; }
                else if (age > 40) { data[3]++; }
            }
            return data;
        }
    };
    return joueurService
})

.factory('terrainService', function () {
    var pers = $T.getPersistence("storage");
    var terrainService = {

        ajouter: function (terrainosef) {
            pers.saveOnLStorage('terrains', terrainosef);
        },

        retirer: function (terrainosef) {
            pers.deleteOnLStorage('terrains', terrainosef);
        },

        retirerTout: function () {
            pers.deleteAll('terrains');
        },
        lister: function () {
            return pers.loadData('terrains');
        }
    };
    return terrainService;

})