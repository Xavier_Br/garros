﻿'use strict';

angular.module('myApp.arbitre.service', [])


.factory('arbitreService', function () {
    var pers = $T.getPersistence("storage");
    var arbitreService = {

        ajouter: function (arbitreosef) {
            pers.saveOnLStorage('arbitres', arbitreosef);
        },

        retirer: function (arbitreosef) {
            pers.deleteOnLStorage('arbitres', arbitreosef);
        },

        retirerTout: function () {
            pers.deleteAll('arbitres');
        },
        lister: function () {
            return pers.loadData('arbitres');
        },
    };
    return arbitreService;
})    