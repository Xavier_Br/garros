﻿'use strict';

angular.module('myApp.terrain.service', [])

.factory('terrainService', function () {
    var pers = $T.getPersistence("storage");
    var terrainService = {

        ajouter: function (terrainosef) {
            pers.saveOnLStorage('terrains', terrainosef);
        },

        retirer: function (terrainosef) {
            pers.deleteOnLStorage('terrains', terrainosef);
        },

        retirerTout: function () {
            pers.deleteAll('terrains');
        },
        lister: function () {
            return pers.loadData('terrains');
        }
    };
    return terrainService;

})
