'use strict';

angular.module('myApp.listeJoueurs', ['ngRoute', 'myApp.listeJoueurs.controller'])

//.directive('hcPieChart', function () {
//	return {
//		restrict: 'E',
//		template: '<div></div>',
//		scope: {
//			title: '@',
//			data: '='
//		},
//		link: function (scope, element) {
//			Highcharts.chart(element[0], {
//				chart: {
//					type: 'pie'
//				},
//				title: {
//					text: scope.title
//				},
//				plotOptions: {
//					pie: {
//						allowPointSelect: true,
//						cursor: 'pointer',
//						dataLabels: {
//							enabled: true,
//							format: '<b>{point.name}</b>: {point.percentage:.1f} %'
//						}
//					}
//				},
//				series: [{
//					data: scope.data
//				}]
//			});
//		}
//	};
//})

.config(['$routeProvider', function ($routeProvider) {
	$routeProvider.when('/listeJoueurs', {
		templateUrl: 'listeJoueurs/listeJoueurs.html',
		controller: 'ListeJoueursCtrl'
	});
}])






