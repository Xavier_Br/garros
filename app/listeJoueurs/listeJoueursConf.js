'use strict';

angular.module('myApp.listeJoueurs', ['ngRoute'])

.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/listeJoueurs', {
        templateUrl: 'listeJoueurs/listeJoueurs.html',
        controller: 'ListeJoueursCtrl'
    });
}])

.controller('ListeJoueursCtrl', ['$scope', function ($scope) {
    $scope.listeJoueurs = $T.getService('joueurService').lister();


    $scope.supprimer = function (joueurid) {
        var joueur = $scope.listeJoueurs[joueurid];
       $scope.listeJoueurs = $T.getService('joueurService').retirer(joueur);
       
    }

}]);