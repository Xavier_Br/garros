﻿angular.module('myApp.listeJoueurs.controller', ['ngRoute', 'myApp.listeJoueurs.service'])

.controller('ListeJoueursCtrl', ['$scope', 'joueurService', function ($scope, joueurService) {
    $scope.listeJoueurs = joueurService.lister();


    $scope.supprimer = function (joueurid) {
        var joueur = $scope.listeJoueurs[joueurid];
        $scope.listeJoueurs = joueurService.retirer(joueur);
    };
    $scope.$watch('listeJoueurs', function () {
        var ctx = $("#myChart");
        $scope.myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["10-20", "20-30", "30-40", "40+"],
                datasets: [{
                    label: '# of Votes',
                    data: joueurService.compter($scope.listeJoueurs),
                    backgroundColor: [
					'rgba(255, 99, 132, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(255, 206, 86, 0.2)',
					'rgba(75, 192, 192, 0.2)'
                    ],
                    borderColor: [
					'rgba(255,99,132,1)',
					'rgba(54, 162, 235, 1)',
					'rgba(255, 206, 86, 1)',
					'rgba(75, 192, 192, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    })
}]);