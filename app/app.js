'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'myApp.joueur',
  'myApp.arbitre',
  'myApp.terrain',
  'myApp.match',
  'myApp.listeJoueurs',
  'myApp.version'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({redirectTo: '/joueur'});
}]);
